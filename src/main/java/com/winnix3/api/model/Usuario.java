package com.winnix3.api.model;

import javax.persistence.Entity;

import lombok.Data;

@Data
//@Entity
public class Usuario {

	private Long id;
	
	private String username;
	
	private String password;
}
