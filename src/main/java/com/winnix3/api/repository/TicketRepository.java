package com.winnix3.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.winnix3.api.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>{

}
