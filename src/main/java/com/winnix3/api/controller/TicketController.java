package com.winnix3.api.controller;

import java.util.List;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.winnix3.api.model.Ticket;
import com.winnix3.api.repository.TicketRepository;

@RestController
@RequestMapping("/api/tickets")
//@Api(value = "winnix3")
public class TicketController {

	@Autowired
	private TicketRepository ticketRepository;
	
	/*@Autowired
	KeycloakRestTemplate template;*/
	
	/*TicketController (TicketRepository repository){
		this.ticketRepository = repository;
	}*/

	
	//@ApiOperation(value = "Mostra os tickets")
	@GetMapping
	public List<Ticket> listar(@RequestHeader (name="Authorization") String token) {
		Object tokenn = null;
	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (authentication != null) {
	    	/*ResponseEntity<String[]> response = template.getForEntity("http://localhost:8080/auth/realms/saiyajin/protocol/openid-connect/userinfo", String[].class);
			System.out.println(response.getBody());
			System.out.println(((Principal) response).getName());*/
	    	
	    	KeycloakPrincipal principal = (KeycloakPrincipal)authentication.getPrincipal();
	        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
	        AccessToken accessToken = session.getToken();
	        String username = accessToken.getPreferredUsername();
	        String emailID = accessToken.getEmail();
	        String lastname = accessToken.getFamilyName();
	        String firstname = accessToken.getGivenName();
	        String realmName = accessToken.getIssuer();
	        AccessToken.Access realmAccess = accessToken.getRealmAccess();
	        System.out.println("id:         "+accessToken.getId());
	        System.out.println("scope:      "+accessToken.getScope());
	        System.out.println("username:   "+username);
	        System.out.println("email:      "+emailID);
	        System.out.println("lastname:   "+lastname);
	        System.out.println("firstname:  "+firstname);
	        System.out.println("realm Name: "+realmName);
	        //System.out.println("roles:      "+accessToken.getRealmAccess().getRoles());
	        System.out.println(accessToken.getOtherClaims().get("authorities"));
	        StringBuilder roles = new StringBuilder();
	        if (accessToken != null /*&& accessToken.getRealmAccess() != null*/) {
	        	//accessToken.getRealmAccess().getRoles().stream().forEach(s -> roles.append(s).append(" "));
	        	((List<String>) accessToken.getOtherClaims().get("authorities")).forEach(s -> roles.append(s).append(" "));
	        }
	        System.out.println("roles: "+roles.toString());
	    	//tokenn = ((org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
	    	//System.out.println("usuario:"+authentication.getPrincipal());
	    	//tokenn = ((Jwt) authentication.getCredentials()).getTokenValue();//.getDetails();
	    	//@AuthenticationPrincipal Usuario logado
	    }
		//System.out.println(tokenn);
		//System.out.println("teste");
		return ticketRepository.findAll();
	}
	
	//@ApiOperation(value = "Cria um ticket")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Ticket adicionar(@RequestBody Ticket ticket) {
		return ticketRepository.save(ticket);
	}
	
	/*
	public List<AppUser> getUsers(OAuth2Authentication auth) {
	    logger.info("CREDENTIALS:" + auth.getCredentials().toString());
	    logger.info("PRINCIPAL:" + auth.getPrincipal().toString());
	    logger.info("OAuth2Request:" + auth.getOAuth2Request());
	    logger.info("UserAuthentication:" + auth.getUserAuthentication());
	    return userService.findAllUsers();
	}*/
	
	/*@GetMapping
	public String userInfo(Model model, Principal principal) {
		ResponseEntity<String[]> response = template.getForEntity(url "realms/unserinfo", String[].class);
		response.getBody();
		return response.getName();//username
	}*/
	
	@GetMapping(path = {"/{id}"})
	public ResponseEntity findById(@PathVariable long id) {
		return ticketRepository.findById(id)
				.map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}
	
	@PutMapping(value="/{id}")
	public ResponseEntity update(@PathVariable("id") long id,
	                                      @RequestBody Ticket ticket) {
	   return ticketRepository.findById(id)
	           .map(record -> {
	               record.setDescription(ticket.getDescription());
	               //record.setEmail(ticket.getEmail());
	               //record.setPhone(ticket.getPhone());
	               Ticket updated = ticketRepository.save(record);
	               return ResponseEntity.ok().body(updated);
	           }).orElse(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping(path ={"/{id}"})
	public ResponseEntity <?> delete(@PathVariable long id) {
	   return ticketRepository.findById(id)
	           .map(record -> {
	               ticketRepository.deleteById(id);
	               return ResponseEntity.ok().build();
	           }).orElse(ResponseEntity.notFound().build());
	}
}
